#!/usr/bin/env python
#   Copyright 2017 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under any FOSS license you like including Apache, BSD, MIT
#

from sys import exit,argv
#from sys import exit,stdin
import fileinput
import re
from string import printable

try:
	from dialog import Dialog
	d = Dialog(dialog="dialog", autowidgetsize=True)
	#import locale
	#locale.setlocale(locale.LC_ALL, '')
except ImportError:
	d = None

SET_PRINTABLE=set(printable)

TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))
#print(len(TRANSLATE_DELETE))

lines_equals2 = []
lines_exclaim3 = []
lines_dotted3 = []
lines_asterisk3 = []
lines_success = []
lines_error = []
lines_tmp = []
lines_lmod = []
lines_logfile = []
lines_path = []
lines_dependenc = []

# chr(32) is space; chr(61) is equals (=)
RE_EQUALS2 = re.compile("{0}{0}{1}".format(chr(61),chr(32)))
# chr(33) is exclamation (!)
RE_EXCLAIM3 = re.compile("{0}{0}{0}".format(chr(33)))
# chr(46) is dot / period
RE_DOTTED3 = re.compile("{0}{0}{0}".format(chr(46)))
# chr(42) is asterisk as in * coded 002A which we will raw prefix obviously
RE_ASTERISK3 = re.compile(r"\{0}\{0}\{0}".format(chr(42)))
RE_SUCCESS = re.compile("success", re.IGNORECASE)
RE_SCEED = re.compile("succeed", re.IGNORECASE)
RE_SFULL = re.compile("successful", re.IGNORECASE)
# chr(58) is colon (:)
RE_ERRORCOLON = re.compile("ERROR{0}".format(chr(58)))
RE_ERROR = re.compile("error", re.IGNORECASE)
# Next two are false positives that we want to NOT class as errors
RE_ANY_ERROR = re.compile("Any error")
RE_OR_MORE_ERROR = re.compile("or more error", re.IGNORECASE)
# chr(47) is forward slash (/)
RE_TMP = re.compile("{0}tmp{0}".format(chr(47)))
RE_LMOD = re.compile(r"Lmod\s+")
RE_LOGFILE = re.compile("be found in the log file", re.IGNORECASE)
# chr(36) is dollar ($)
RE_PATH = re.compile("{0}{1}[A-Z]".format(chr(32),chr(36)))
RE_DEPENDENC = re.compile("dependenc", re.IGNORECASE)
RE_FSEASYBUILDFS = re.compile("{0}easybuild{0}".format(chr(47)))

lines_eb = []


def lines_joined(lines,joiner=chr(10)):
        joined = joiner.join(lines)
        return joined
        

def process_lines(lines,linelimit=0,problem_count_max=0,verbosity=0):
	""" Process the lines
	problem_count_max of zero (default) means 'no maximum' so never break out
	of loop because of controlled problem.
	"""
        global lines_eb
	lines_with_problems = 0
	line_rc = 999

	for idx,line in enumerate(lines):

		#print("processing line %d" % idx)
		line_rc = 0


		if set(line).issubset(SET_PRINTABLE):
			pass
		else:
			line_rc = 110
			lines_with_problems += 1
			print('line %s has one or more unprintables!' % idx)
						
			line_translated = line.translate(None,TRANSLATE_DELETE)
			if len(line) != len(line_translated):
				line_rc = 120
				lines_with_problems += 1
				print('line %s has tab or other low range chr()' % idx)

		if problem_count_max > 0:
			if lines_with_problems > problem_count_max:
				return line_rc

		if linelimit > 0:
			if idx >= linelimit:
				return 200

                lines_eb.append(line)

		if RE_EQUALS2.match(line):
			lines_equals2.append(line)
		elif RE_EXCLAIM3.match(line):
			lines_exclaim3.append(line)
		elif RE_ERRORCOLON.match(line):
			lines_error.append(line)
		elif RE_PATH.match(line):
			lines_path.append(line)
		else:
			pass

		if RE_ERROR.search(line):
			if RE_ANY_ERROR.search(line):
				pass
			elif RE_OR_MORE_ERROR.search(line):
				pass
			else:
				lines_error.append(line)
		elif RE_SUCCESS.search(line):
			if RE_SFULL.search(line):
				# This else would also catch unsuccessful :(
				lines_success.append(line)
			else:
				lines_success.append(line)
		elif RE_SCEED.search(line):
				# This else would also catch 'did not succeed' :(
				lines_success.append(line)
		else:
			pass

		if RE_DOTTED3.search(line):
			lines_dotted3.append(line)
		if RE_ASTERISK3.search(line):
			lines_asterisk3.append(line)
		if RE_ASTERISK3.search(line):
			lines_asterisk3.append(line)
		if RE_TMP.search(line):
			lines_tmp.append(line)
		if RE_LMOD.search(line):
			lines_lmod.append(line)
		if RE_LOGFILE.search(line):
			lines_logfile.append(line)
		if RE_DEPENDENC.search(line):
			lines_dependenc.append(line)

	return 0


def process_fileinput(finput):
        #lines_eb = finput.strip().split('\n')
	return process_lines(finput.strip().split('\n'))


if __name__ == "__main__":
	#lret = process_lines(fileinput.input(inplace=False),0,0,1)
	lret = process_lines(fileinput.input(inplace=False))
	#lret = process_lines(fileinput.input(inplace=False),0,0,0)

	logfile = None
	logfilename = None
	ylabel = 'Finish'
	for line in lines_logfile:
		if RE_FSEASYBUILDFS.search(line):
			line_array = line.split()
			logfile_fullpath = line_array[-1]
			logfile_array = logfile_fullpath.split(chr(47))
			logfilename = logfile_array[-1]
			ylabel = 'ShowLog'

	if d is None:
		exit(0)

	d.set_background_title("EasyBuild results")
	eb_feedback = "Successes:{0}".format('\n')
	eb_feedback = "{0}----------{1}".format(eb_feedback,'\n')
	for line in lines_success:
		eb_feedback = "{0}{1}".format(eb_feedback,line)
	eb_feedback = "{0}Errors:{1}".format(eb_feedback,'\n')
	eb_feedback = "{0}-------{1}".format(eb_feedback,'\n')
	for line in lines_error:
		eb_feedback = "{0}{1}".format(eb_feedback,line)

	if ylabel.startswith('Fin'):
		pass
	else:
		if logfilename is None:
			eb_feedback = "{0}Logfile:{1}".format(eb_feedback,'\n')
		else:
			eb_feedback = "{0}Logfile: {1}{2}".format(eb_feedback,
                                                                  logfilename,
                                                                  '\n')
		eb_feedback = "{0}--------{1}".format(eb_feedback,'\n')
		for line in lines_logfile:
			eb_feedback = "{0}{1}".format(eb_feedback,line)

	nlabel = 'Quit'
	dpress = d.yesno(eb_feedback,yes_label=ylabel,no_label=nlabel,help_button=False)

	if logfilename is None:
		exit
	elif dpress == d.OK:
		#print(logfilename)
                try:
                        with open(logfile_fullpath,'r') as logfile:
                                try:
                                        lines = name_file.readlines()
                                except:
                                        lines = None
                except:
                        lines = lines_eb

		if lines is not None:
                        lines_feedback = lines_joined(lines)
			#ylabel = 'Finish'
			dpress = d.yesno(lines_feedback,yes_label='Finish',
                                         no_label=nlabel,help_button=False)

# == processing EasyBuild easyconfig



