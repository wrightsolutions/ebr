#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017, Gary Wright http://www.wrightsolutions.co.uk/contact
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Filter output of module avail

import csv
#from collections import defaultdict,namedtuple,OrderedDict
from collections import namedtuple
import re
from sys import exit,argv
from subprocess import Popen,PIPE
import shlex
#from __future__ import with_statement

__version__ = '0.1'

#BUFSIZE=16384
BUFSIZE=8192
FS=chr(47)	# forward slash
DQ=chr(39)	# double quote
SQ=chr(39)	# single quote
TABBY=chr(9)
MSH=chr(45)	# minus sign / hyphen
COLON=chr(58)	# colon (:)
SCOL=chr(59)	# semicolon (;)
EQUALS=chr(61)
#RE_PARENTHESIZED_WORD = re.compile(r'\(\w+\)')
PARENTHS_OPEN = chr(40)
PARENTHS_CLOSE = chr(41)
# echo '---
RE_ECHO_SQ = re.compile("echo\s+{0}".format(SQ))
#RE_MSH3PLUS = re.compile("{0}{1}3,{2}".format(MSH,PARENTHS_OPEN,PARENTHS_CLOSE))
#RE_MSH3PLUS = re.compile("-{3,}")
RE_MSH3PLUS = re.compile("echo\s+{0}{1}{2}3,{3}".format(SQ,MSH,PARENTHS_OPEN,PARENTHS_CLOSE))
RE_MSH3PLUS = re.compile("echo\s+{0}{1}".format(SQ,MSH,PARENTHS_OPEN,PARENTHS_CLOSE))
RE_PYTHON = re.compile('ython')
RE_SQUARE_BRACKETED = re.compile("^\[.*\]".format(chr(91),chr(93)))
RE_SQUARE_BRACKETED_LOOSE = re.compile(r"\s*\[.*\]".format(chr(91),chr(93)))
RE_UPPERCASE_EQUALS_EXPORT = re.compile(r"export\s+.*".format(EQUALS))
RE_UPPERCASE_EXPORT_ONLY = re.compile("export\s+.*{0}".format(SCOL))
RE_UPPERCASE_EXPORT_ONLY = re.compile(r"export\s+[_0-9A-Z]+{0}".format(SCOL))
RE_UPPERCASE_EQUALS = re.compile(r"[^_\W]{0}".format(EQUALS))
RE_UPPERCASE_EQUALS = re.compile(r"[^_\W{0}]".format(EQUALS))
RE_UPPERCASE_EQUALS_DQ = re.compile(r"[^_\W{0}{1}]".format(EQUALS,DQ))
RE_UPPERCASE_EQUALS_DQ = re.compile(r"[_0-9A-Z]+{0}".format(DQ))
#RE_FS_LMOD = re.compile(r"^echo\( {0}\)*.*{1}.*{0}".format(SQ,FS))
#RE_FS_LMOD = re.compile("{0}.*{1}.*{0}".format(SQ,FS))
RE_FS_LMOD = re.compile("{0}\w+{1}[{2}\.\w]*{0}".format(SQ,FS,MSH))
RE_MODULEFILES_CORE = re.compile("modulefiles{0}Core\s+".format(FS))
RE_MODULE_TABLE = re.compile("_ModuleT")
RE_DEFAULT = re.compile(r"\{0}D\{1}".format(PARENTHS_OPEN,PARENTHS_CLOSE))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>\( {0}\)*)(?P<wordfs>\w+{1}\w+)(\( {0}\)*)(?P<rem>.*)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>\( {0}\)*)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>( {0})*)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>( {0})*)\W*(?P<wordfs>\w*{1}\w+)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>( {0})*)\W*(?P<wordfs>\w*{1}\w+.*)(?P<sq2>{0}+)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>( {0})*)\W*(?P<wordfs>\w*{1}\w+.*)(?P<dashes>-{0}+?)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>( {0})*)\W*(?P<wordfs>\w*{1}\w+.*)?(?P<dashes>-)(?P<sq2>{0}+)".format(SQ,FS))
RE_FS_SPLIT = re.compile(r"(?P<echospace>echo\s+)(?P<spacesq>( {0})*)\W*(?P<wordfs>\w*{1}\w+.*)?(?P<sqc>({0};)+)".format(SQ,FS))

def lines_to_list_of_tuples(lines):
	list_of_tuples = []
	LmodTuple = namedtuple('LmodTuple', ['linenum','line','name_with_fs','wordfs','word_last'])
	markerline_msh_count = 0
	#clines = csv.reader(lines)
	lines_filtered = []
	for idx,line in enumerate(lines):

		if idx < 99999:
			if RE_UPPERCASE_EXPORT_ONLY.match(line):
				#print(line[0:5])
				continue
			elif RE_UPPERCASE_EQUALS_DQ.match(line):
				#print(line[0:10])
				continue
			elif RE_MODULE_TABLE.match(line):
				#print(line[0:12])
				continue
			else:
				pass

		if RE_MODULEFILES_CORE.search(line):
			break

		if RE_MSH3PLUS.match(line):
			#print(line[0:2])
			markerline_msh_count += 1
			continue

		#print(line)
		name_with_fs = None
		cline = RE_FS_LMOD.findall(line)
		if cline:
			# Have cline populated
			name_with_fs = cline[0].strip(DQ).strip(SQ)
			if len(name_with_fs) > 3:
				#print(name_with_fs)
				pass
		efoundall = RE_FS_SPLIT.finditer(line)
		if efoundall:
			for found in efoundall:
				#if found.group('dashes'))
				#print(found.group('echospace'))
				#print(found.group('spacesq'))
				wordfs = found.group('wordfs')
				if wordfs is None:
					#print(line,line)
					continue
				else:
					word_last = wordfs.split()[-1].strip().strip(SQ)
					#pos2 = found.end('wordfs')
					#print(pos2,line[pos2:])
					#tup = LmodTuple('linenum','line','name_with_fs','wordfs','word_last')
					tup = LmodTuple(idx,line,name_with_fs,wordfs,word_last)
					list_of_tuples.append(tup)
				#print(found.group('dashes'))
				#print(found.group('sq2'))
				#print(found.group('wordpenum'))
				#print(found.group('sqc'))
				pass
	print(markerline_msh_count)
	print(len(lines_filtered))
	for item in line:
		csvline = item[0]
		#print(csvline)
		cline = csv.reader(csvline)
		for field in cline:
			#print(field)
			if FS in field:
				pass
				#print(field)
	return list_of_tuples


def process_lines(lines,re_compiled=None,print_flag=True):
	tuplist = lines_to_list_of_tuples(lines)
	for tup in tuplist:
		if re_compiled is None:
			if print_flag:
				print(tup.name_with_fs,tup.word_last,tup.wordfs)
		if RE_PYTHON.search(tup.line):
			pass
			#print(tup.line.split(SQ))
	return


if __name__ == '__main__':
	exit_rc = 0
	pyver = None
	if len(argv) > 1:
		pyver = argv[1]

	# chr(92) is back slash
	#acmd = "module avail"
	#acmd = "/usr/share/lmod/lmod/libexec/lmod --redirect --dumpversion avail"
	acmd = "/usr/share/lmod/lmod/libexec/lmod --redirect avail"
	lexxed = shlex.split(acmd)
	#print(lexxed)
	subout = (Popen(lexxed,bufsize=BUFSIZE,stdout=PIPE)).stdout
	process_lines(subout.readlines())
	exit(exit_rc)

