#!/bin/sh
#   Copyright 2017 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under any FOSS license you like including Apache, BSD, MIT
#

# eb binutils-2.28-GCCcore-6.3.0.eb -r
# eb binutils-2.28-GCCcore-6.3.0.eb -r -f
# eb -x binutils-2.28-GCCcore-6.3.0.eb -r

SUFFIX_=' -r'

[ $# -lt 1 ] && exit 101

RECIPE_='binutils-2.28abcfedghilkj.eb'
if [ ! -z "$1" ]; then
	RECIPE_=${1}
fi

[ -r "${RECIPE_}" ] && exit 110

module load EasyBuild
BIN_=$(which eb)
WRC_=$?
printf "which returned %s\n" $WRC_
[ ${WRC_} -gt 0 ] && exit ${WRC_}
printf "%s %s $s\n" "$BIN_ $RECIPE_ $SUFFIX_"
PARSER_='ebr_parse.py'
# Next we set a shell option BEFORE making our piped call
set -o pipefail
${BIN_} ${RECIPE_} ${SUFFIX_} | ${PARSER_}
BRC_=$?

# == processing EasyBuild easyconfig
