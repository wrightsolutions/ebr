#!/bin/sh
python -c "import os; \
import jinja2 as j2; \
j2env=j2.Environment(loader=j2.FileSystemLoader('.')); \
dict1 = {'name_in_easyblock': 'sympy',
'version_in_easyblock': '1.0',
'homepage_description': 'SymPy is a Python library for symbolic mathematics. It aims to  become a full-featured computer algebra system (CAS) while keeping the code as simple as possible in order to be comprehensible and easily extensible. SymPy is written entirely in Python and does not require any external libraries.',
'patchfile': 'sympy-1.0_tests-unicode.patch',
'homepage_url': 'http://sympy.org/'
}; \
print j2env.get_template('python_package_pypi_source_template.j2').render(dict1)"
RC_=$?
exit $RC_

