#!/bin/sh
python -c "import os; \
import jinja2 as j2; \
j2env=j2.Environment(loader=j2.FileSystemLoader('.')); \
dict1 = {'name_in_easyblock': 'sympy'
}; \
print j2env.get_template('python_package_pypi_source_template.j2').render(dict1)"
RC_=$?
exit $RC_